### Edo's face-to-face meeting slides:  https://indico.cern.ch/event/800153/contributions/3340883/attachments/1815986/2968040/20190321F2F3.pdf
### EUTelescope documentation:          https://arxiv.org/pdf/1707.04535.pdf

### so far only found to work on slc6, so
ssh -Y lxplus6

### the home of EUTelescope
mkdir -p myEUTelescopeSetup
cd myEUTelescopeSetup

### env configuration
setupATLAS
lsetup "gcc gcc620_x86_64_slc6"
source /cvmfs/sft.cern.ch/lcg/releases/LCG_88/ROOT/6.08.06/x86_64-slc6-gcc62-opt/ROOT-env.sh
export EUTELESCOPE="$(pwd)/v01-19-02/ilcutil/v01-02-01"
export ILCSOFT="$(pwd)"

### get the repo
mkdir -p $ILCSOFT
cd $ILCSOFT
git clone -b dev-base https://github.com/eutelescope/ilcinstall $ILCSOFT/ilcinstall
#scl enable rh-git29 bash   #I think this just messes things up!

### open the file examples/eutelescope/release-standalone.cfg and replace the lines 157 and 158 with: 
#-->  ROOT_PATH= "/cvmfs/sft.cern.ch/lcg/releases/LCG_88/ROOT/6.08.06/x86_64-slc6-gcc62-opt/"
#-->  ilcsoft.use( ROOT( ROOT_PATH ))

### open the file $EUTELESCOPE/build_env.sh and remove :$MARLIN_DLL at the end of MARLIN_DLL on line 59, i.e. change it to (mind ...):
#-->  export MARLIN_DLL="/afs/cern.ch/.../EUTelescope/v01-19-02/Eutelescope/master/lib/libEutelescope.so:$EUTELESCOPE/lib/libEutelProcessors.so:$EUTELESCOPE/lib/libEutelReaders.so:$EUDAQ/lib/libNativeReader.so"
### while you're at, in order to avoid Marlin errors at the end of otherwise successful runs, remove the $EUTELESCOPE/lib/libEutelReaders.so: block from the export above.

### install
cd ilcinstall
./ilcsoft-install -i examples/eutelescope/release-standalone.cfg
cd ..

### stuff for finding jobsub if it is not found
#cd /afs/cern.ch/work/e/edreyer/public/EUTelescope/v01-19-02/Eutelescope/master
#rm -r build
#mkdir build
#cd build
#cmake ..
#make -j 4
#make install -j 4

### stuff for getting pyLCIO to work (in case of the following error:
###     Error in <TUnixSystem::FindDynamicLibrary>: /afs/cern.ch/.../EUTelescope/v01-19-02/lcio/master/lib/liblcioDict.so does not exist in ...<PATH>
### the liblcioDict.so dictionary seems to be created and put in a cute little lib directory inside the build directory of lcio, i.e. lcio/master/build/lib/ . If it exists there, you can simply create a symlink:
###     ln -s pathToTheExistingDict pathOfTheMissingDict
### If it does not exist there, you should try cmaking inside lcio again inside lcio/master/build/ as follows:
###     cmake -DBUILD_ROOTDICT=ON ..
###     make
### Then check if it exists. You can get some more hints at https://github.com/iLCSoft/LCIO/blob/27c67df622c934f3258a38e167b4b358781f306a/examples/python/README

### environment
source ${ILCSOFT}/v01-19-02/ILCSoft.cmake.env.sh
source ${ILCSOFT}/v01-19-02/init_ilcsoft.sh
source $EUTELESCOPE/build_env.sh

#alias jobsub="python /afs/cern.ch/work/e/edreyer/public/EUTelescope/v01-19-02/Eutelescope/master/jobsub/jobsub.py"

### test dump event (from EUDAQ conversion)
dumpevent run000180.slcio 100

### test jobs. Follow the README in v01-19-02/Eutelescope/master/jobsub/examples/GBL_noDUT
cd "${ILCSOFT}/Eutelescope/master/jobsub/examples/GBL_noDUT"
jobsub -c config_DESY2018.cfg -csv runlist.csv -g noisypixel 701
