# Notes on EUTelescope analysis for DESY April 2019 test beam

Detailed information on how to use EUTELESCOPE software is available at http://eutelescope.desy.de

## Prerequisites

Source the build_env.sh from the EUTel main directory to set the correct environment:

`source $EUTELESCOPE/build_env.sh`

## Running

The analysis we want to perform is controlled by a config file (config.cfg), a csv-table (runlist.csv) and steering file templates (\*.xml), all located in the working directory.

You can set the variables for your working directory here as well as input parameters for the different processors.


There a two different versions, one for raw-data taken with EUDAQ1 and one with already converted data files taken with EUDAQ2.

### EUDAQ2 version

You should be able to run the analysis now. For this, execute the following commands one after another and check for each of the processors to successfully finish.

```
jobsub -c config.cfg -csv runlist.csv -g noisypixel 190
jobsub -c config.cfg -csv runlist.csv -g clustering 190
jobsub -c config.cfg -csv runlist.csv -g hitmaker 190
```

Use output of first hitmaker (gear file with \_pre) as input in runlist for next hitmaker...

```
jobsub -c config.cfg -csv runlist.csv -g hitmaker 190
jobsub -c config.cfg -csv runlist.csv -g alignGBL1 190
jobsub -c config.cfg -csv runlist.csv -g alignGBL2 190
jobsub -c config.cfg -csv runlist.csv -g alignGBL3 190
jobsub -c config.cfg -csv runlist.csv -g fitGBL 190
```

## Output

At every step a ROOT file is created, containing a set of histograms, which you can be found at output/histograms/ after completion.
