### following steps from http://eudaq.github.io
### github docs: https://github.com/eudaq/eudaq/blob/master/README.md#execution
### eudaq2 user manual: http://eudaq.github.io/manual/EUDAQUserManual_v2.pdf
### eu-documentation: https://arxiv.org/pdf/1707.04535.pdf

### so far only found to work on slc6, so
ssh -Y lxplus6

### get the code
git clone https://github.com/eudaq/eudaq.git eudaq 
cd eudaq
git checkout v2.2.0

### configure
setupATLAS
lsetup "cmake"
#lsetup "gcc gcc620_x86_64_slc6"
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
mkdir build
cd build
cmake -D USER_ITKSTRIP_BUILD=ON ..
make install

### run raw->slcio conversion
./bin/euCliConverter -i /eos/user/i/itktb194/EUDAQ-RAW/run000180_190430012947.raw -o run000180.slcio
